import Head from 'next/head'
import Link from 'next/link'
import Layout, { siteTitle } from '../components/layout'
import utilStyles from '../styles/utils.module.css'
import { randomNumbers } from '../utils/general'
import { getAllPostData } from '../lib/posts'
import Accordion from '../components/accordion/accordion'

function AccordionContent({ identifier, body }) {
  return (
   <>
    <br />
    <small className={utilStyles.lightText}>
      {body}
    </small>
    <br />
    <div style={{ margin: '1em 0 1em' }}>
      <Link href={`/posts/${identifier}`}>
        <a>{'See more details'}</a>
      </Link>
    </div>
   </>
  )
}
export default function Home({ posts }) {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <p className={utilStyles.justifyText}>
          Najhu wipu koktakeke wo kodul mejsafom gipo gevijav wahgeliz oggezora bifum bad karez dej ekebipa vep.
          Mudejomu etedpe le waipave cofhom di zilo fivnas ruhetizun kime nilimop peiv. Ac gato minec nu fo ol limu
          um hucjuwup ula pose zefop irhekal eh hojerzuw.
        </p>
        <p>
          (This is a sample website - you’ll be building a site like this on{' '}
          <a href="https://nextjs.org/learn">our Next.js tutorial</a>.)
        </p>
      </section>

      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>
          {'Blog'}
        </h2>
          {posts && posts.map(({ id, title, body }) => (
            <Accordion
              key={id}
              title={title}
              content={(
                <AccordionContent
                  identifier={id}
                  body={body}
                />
              )}
            />
          ))}
      </section>
    </Layout>
  )
}

export async function getStaticProps() {
  const allPosts = await getAllPostData();
  const posts = allPosts.filter(
    ({ id }) => id >= randomNumbers(0, allPosts.length / 4) && id <= randomNumbers(0, allPosts.length / 2)
  );

  return {
    props: {
      posts,
    }
  }
}