import React, { useState, useRef } from 'react'
import Chevron from './Chevron';
import accordionStyles from './accordion.module.css'

export default function Accordion(props) {
  const [ setActive, setActiveState ] = useState('');
  const [ setHeight, setHeightState ] = useState('0px');
  const [ setRotate, setRotateState ] = useState(accordionStyles.accordion__icon);
  const content = useRef(null);

  function toggleAccordion() {
    setActiveState(setActive === '' ? accordionStyles.active : '');
    setHeightState(
      setActive === accordionStyles.active ? '0px' : `${content.current.scrollHeight}px`
    );
    setRotateState(
      setActive === accordionStyles.active ?
        accordionStyles.accordion__icon :
        `${accordionStyles.accordion__icon} ${accordionStyles.rotate}`
    );
  }

  return (
    <div className={accordionStyles.accordion__section}>
      <button
        className={`${accordionStyles.accordion} ${setActive}`}
        onClick={toggleAccordion}
      >
        <p
          className={accordionStyles.accordion__title}>
           {props.title}
        </p>
        <Chevron className={`${setRotate}`} width={10} fill={"#777"} />
      </button>
      <div
        ref={content}
        style={{ maxHeight: `${setHeight}` }}
        className={accordionStyles.accordion__content}
      >
        <div
          className={accordionStyles.accordion__text}
        >
          {props.content}
        </div>
      </div>
    </div>
  );
}
