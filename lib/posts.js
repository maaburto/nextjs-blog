const BASE_API_URL = process.env.BASE_API_URL;
const entity = 'posts'

export async function getPostData(id) {
  const res = await fetch(`${BASE_API_URL}/${entity}/${id}`)
  const post = await res.json()

  // Combine the data with the id
  return post
}

export async function getAllPostIds() {
  const posts = await getPosts()
  return (await posts.json()).map(({ id }) => ({
    params: {
      id: id.toString(),
    }
  }));
}

export async function getAllPostData() {
  const resPosts = await getPosts()

  return await resPosts.json()
}

async function getPosts() {
  return await fetch(`${BASE_API_URL}/${entity}`)
}